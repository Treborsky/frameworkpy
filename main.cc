//
//  Author: Robert K
//
//  Date: 15.04.2022
//
//  Name: main.cc
//
//  Description: entry point for a c++ program used in frameworkpy development
//

#include <iostream>
#include "library.h"

int main() {
    std::cout << "Sample application for frameworkpy development" << std::endl;

    const Point a(0.0, 0.0);
    const Point b(0.0, 1.0);

    const double distance_a_to_b = calculate_distance_between_two_points(a, b);

    std::cout << "Distance between points\n"
    << "a = (" << a.x() << ", " << a.y() << ")\n"
    << "b = (" << b.x() << ", " << b.y() << ")\n"
    << "equals = " << distance_a_to_b << std::endl;

}
