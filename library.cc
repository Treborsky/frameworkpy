//
//  Author: Robert K
//
//  Date: 15.04.2022
//
//  Name: library.cc
//
//  Description: library.h source file for frameworkpy development
//

#include "library.h"
#include <cmath>

const double Point::x() const {
    return m_x;
}

const double Point::y() const {
    return m_y;
}

const double calculate_distance_between_two_points(const Point& a, const Point& b) {
    const double dx = b.x() - a.x();
    const double dy = b.y() - a.y();
    const double distance_from_a_to_b = std::sqrt(
        std::pow(dx, 2.0) + std::pow(dy, 2.0)
    );
    return distance_from_a_to_b;
}
