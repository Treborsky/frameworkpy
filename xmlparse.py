#
#   Author: Robert K
#
#   Date: 16.04.2022
#
#   Name: xmlparse.py
#
#   Description: XML parser specifically made for frameworkpy
#
from gcclib import GCCconfigurationStruct

def parse_conf_file(path_to_conf_file='./', conf_object: GCCconfigurationStruct):

    