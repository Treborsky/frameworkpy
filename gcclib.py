#
#   Author: Robert K
#
#   Date: 16.04.2022
#
#   Name: gcclib.py
#
#   Description: GCC calls for build steps with frameworkpy
#

import subprocess

def call_gcc(path_to_gcc='.', command='--version'):
    gcc_process = subprocess.Popen(path_to_gcc + ' ' + command)
    print(gcc_process.args)
    gcc_process.wait()
    print('[RUN]: GCC call complete')

