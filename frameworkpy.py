#
#   Author: Robert K
#
#   Date: 14.04.2022
#
#   Name: frameworkpy.py
#
#   Description: entry point for my framework tool
#

from flib import find_source_files, find_conf_file
from flib import ConfigurationFileSearchFailedException, SourceFileSearchFailedException
from gcclib import call_gcc

if __name__ == "__main__":
    print("[RUN]: Searching for C++ source files...")

    try:
        source_files = find_source_files()                         # search for C++ source files
        print('[OK ]: Found C++ source files')
        conf_file = find_conf_file()                               # find configuration file
        print('[OK ]: Found configuration file at: {}'.format(conf_file))
        print('[RUN]: Executing compilation step')

        gcc_args = '-o prog '
        for src_file in source_files:
            gcc_args += src_file + ' '
        
        print('gcc_args: {}'.format(gcc_args))
        call_gcc('C:\\MinGW\\bin\\g++.exe', gcc_args)

    except (SourceFileSearchFailedException, ConfigurationFileSearchFailedException) as e:
        print(e.msg)

    print('[RUN]: Exiting.')
