//
//  Author: Robert K
//
//  Date: 15.04.2022
//
//  Name: library.h
//
//  Description: example C++ library for frameworkpy development
//

class Point {
    public:
        Point(double x=0.0, double y=0.0) : m_x(x), m_y(y) {}
        Point(const Point& other) : m_x(other.x()), m_y(other.y()) {}
        const double x() const;
        const double y() const;
    private:
        double m_x;
        double m_y;
};

const double calculate_distance_between_two_points(const Point&, const Point&);
