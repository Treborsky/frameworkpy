#
#   Author: Robert K
#
#   Date: 14.04.2022
#
#   Name: flib.py
#
#   Description: custom file utility library for my framework 
#

import os
from typing import List


class SourceFileSearchFailedException(Exception):

    def __init__(self, msg):
        super().__init__(msg)
        self.msg = msg

class ConfigurationFileSearchFailedException(Exception):

    def __init__(self, msg):
        super().__init__(msg)
        self.msg = msg


def find_conf_file(path='./') -> str:
    working_directory_files = os.listdir(path)

    if 'frameworkpy.conf.xml' in working_directory_files:
        return path + 'frameworkpy.conf.xml'
    else:
        raise ConfigurationFileSearchFailedException('No configuration file found in provided path.')


def find_source_files(path='.') -> List[str]:
    source_directory_files = os.listdir(path)
    
    filter_names = ['.cc', '.cpp']#, '.h', '.hh', '.hpp']
    filtered_files = []
    for file in source_directory_files:
        for name in filter_names:
            if name in file:
                filtered_files.append(file)

    if not filtered_files:
        raise SourceFileSearchFailedException('No file in {} directory'.format({'script launch' if path == '.' else path}))
                                            # TODO: [priority:low] figure out how to use f'' in the exception throw

    filtered_files = ['' + file for file in filtered_files]

    return filtered_files
