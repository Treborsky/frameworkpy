# frameworkpy

A simple python-based framework for building C++ project containing a couple source files.

# usage

The only usecase predicted for this project is tiny projects, where the developer knows what's going on during the build process. All this "framework" provides is a simple cli.

To build your program, run the python script **frameworkpy.py**.

# contributions

I'd love for other people to propose more functionality to this project, as I wrote it in around 3 hours, while bored. I truly believe it could be expanded into a quite useful open-source project.
